//
//  CastMember.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

struct CastMember{
    var id : Int?
    var originalName : String?
    var castName : String?
    var picturePath : String?
    
    init(json: [String:Any]){
      originalName = json["name"] as? String
      id = json["id"] as? Int
      castName = json["character"] as? String
      picturePath = "https://image.tmdb.org/t/p/w200/"
      picturePath?.append((json["profile_path"] as? String) ?? "")
    }
}
