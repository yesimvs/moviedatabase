//
//  File.swift
//  ProjectMovie
//
//  Created by Yesim on 1/16/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

protocol ViewModelDelegate: class {
    func willLoadData()
    func didLoadData()
}

protocol ViewModelType {
    var delegate: ViewModelDelegate? { get set }
}
