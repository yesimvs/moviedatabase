//
//  Movie.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

struct Movie{
    var id : Int?
    var title : String?
    var imagePath : String?
    
    init(json: [String:Any]){
      title = json["title"] as? String
      id = json["id"] as? Int
      imagePath = "https://image.tmdb.org/t/p/w200/"
      imagePath?.append((json["poster_path"] as? String) ?? "")
    }
}
