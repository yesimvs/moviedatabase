//
//  Video.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

struct Video : Codable {
    var key : String?
    var imagePath : String?
    
    init(json: [String:Any]){
      key = json["key"] as? String
      imagePath = "https://img.youtube.com/vi/\(key ?? "")/0.jpg"
    }
}
