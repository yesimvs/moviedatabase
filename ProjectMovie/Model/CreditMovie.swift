//
//  File.swift
//  ProjectMovie
//
//  Created by Yesim on 1/16/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

struct CreditMovie : Codable {
    var id : Int?
    var title : String?
    var imagePath : String?
    var castName : String?
    
    init(json: [String:Any]){
      title = json["title"] as? String
      id = json["id"] as? Int
      castName = json["character"] as? String
      imagePath = "https://image.tmdb.org/t/p/w200/"
      imagePath?.append((json["poster_path"] as? String) ?? "")
    }
}
