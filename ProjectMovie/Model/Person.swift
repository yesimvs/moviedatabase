//
//  Person.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

struct Person{
    var id : Int?
    var name : String?
    var picturePath : String?
    var biography : String?
    var credits : [CreditMovie]?
    
    
    init(json: [String:Any]){
      name = json["name"] as? String
      biography = json["biography"] as? String
      id = json["id"] as? Int
      picturePath = "https://image.tmdb.org/t/p/w200/"
      picturePath?.append((json["profile_path"] as? String) ?? "")
    }


}
