//
//  ResultItemModel.swift
//  ProjectMovie
//
//  Created by Yesim on 1/16/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

enum MediaType {
  case movie, castMember
}

protocol ResultItem {
  var title: String { get }
  var mediaType: MediaType { get }
}

struct ResultItemModel: ResultItem {

   let title: String
   let mediaType: MediaType

   init(title: String, mediaType: MediaType) {
     self.title = title
     self.mediaType = mediaType
   }
}
