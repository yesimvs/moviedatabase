//
//  Detail.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

struct Detail{
    var id : Int?
    var title : String?
    var summary : String?
    var rating : Double?
    var coverPhoto : String?
    var videos : [Video]?
    var cast : [CastMember]?
    
    init(json: [String:Any]){
      summary = json["overview"] as? String
      rating = json["vote_average"] as? Double
      title = json["title"] as? String
      id = json["id"] as? Int
      coverPhoto = "https://image.tmdb.org/t/p/w200/"
      coverPhoto?.append((json["poster_path"] as? String) ?? "")
    }
}
