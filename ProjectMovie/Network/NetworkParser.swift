//
//  NetworkParser.swift
//  ProjectMovie
//
//  Created by Yesim on 1/15/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation
import SwiftyJSON

struct NetworkParser{

    static func parseMostPopular(_ data:Dictionary<String, AnyObject>) -> [Movie] {
        var array = [Movie]()
        let jsonData = JSON(data)
        if let resultData = jsonData["results"].arrayObject {
            let result = resultData as! [[String:AnyObject]]
            for element in result {
                let movie = Movie(json: element)
                array.append(movie)
            }
        }
        return array
    }
    
    static func parseMovieDetail(_ data:Dictionary<String, AnyObject>) -> Detail {
 
        let detail = Detail(json: data)
        return detail
    }

    static func parsePersonDetail(_ data:Dictionary<String, AnyObject>) -> Person? {
 
        let person = Person(json: data)
        return person
    }
    
    static func parseMovieCast(_ data:Dictionary<String, AnyObject>) -> [CastMember] {
        var cast = [CastMember]()
        let jsonData = JSON(data)
        if let resultData = jsonData["cast"].arrayObject {
            let result = resultData as! [[String:AnyObject]]
            for element in result {
                let member = CastMember(json: element)
                cast.append(member)
            }
        }
        return cast
    }
    
    static func parseMovieVideos(_ data:Dictionary<String, AnyObject>) -> [Video] {
        var videos = [Video]()
        let jsonData = JSON(data)
        if let resultData = jsonData["results"].arrayObject {
            let result = resultData as! [[String:AnyObject]]
            for element in result {
                let video = Video(json: element)
                videos.append(video)
            }
        }
        return videos
    }
    
    static func parsePersonCredits(_ data:Dictionary<String, AnyObject>) -> [CreditMovie] {
        var credits = [CreditMovie]()
        let jsonData = JSON(data)
        if let resultData = jsonData["cast"].arrayObject {
            let result = resultData as! [[String:AnyObject]]
            for element in result {
    
                let movie = CreditMovie(json: element)
                credits.append(movie)
            }
        }
        return credits
    }

    static func parseSearchResult(_ data:Dictionary<String, AnyObject>) -> [Any] {

         var array = [Any]()
         let jsonData = JSON(data)
         if let resultData = jsonData["results"].arrayObject {
            let result = resultData as! [[String:AnyObject]]
              for element in result {
         
                if((element["media_type"]?.isEqual("person"))!){
                    let person = CastMember(json: element)
                    array.append(person)
                }
                else if((element["media_type"]?.isEqual("movie"))!){
                    let movie = Movie(json: element)
                    array.append(movie)
                }
            }
             }
          return array
    }

        
    
}


