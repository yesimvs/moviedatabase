//
//  NetworkManager.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import PromiseKit

struct NetworkManager: NetworkDataSource {

    static let baseURL = "https://api.themoviedb.org/3/%@language=en-US&api_key=8e14948e471b162969d018abe7584724"
    
    func getMostPopularMovies(page: Int, completion: @escaping ([Movie]) ->()){
        makeAPICall(path: "movie/popular?page=\(page)&") { (data) in
            completion(NetworkParser.parseMostPopular(data))
   }
    }
    
    func getMovieDetail(id: Int, completion: @escaping (Detail) -> ()) {        
        makeAPICall(path: "movie/\(id)?") { (data) in
            completion(NetworkParser.parseMovieDetail(data))
        }
    }
    
    func getMovieCast(id: Int, completion: @escaping ([CastMember]) -> ()) {
        makeAPICall(path: "movie/\(id)/credits?") { (data) in
             completion(NetworkParser.parseMovieCast(data))
        }
    }
    
    func getMovieVideos(id: Int, completion: @escaping ([Video]) -> ()) {
        makeAPICall(path: "movie/\(id)/videos?") { (data) in
             completion(NetworkParser.parseMovieVideos(data))
        }
    }
    
    func getPersonDetail(id: Int, completion: @escaping (Person?) -> ()) {
        makeAPICall(path: "person/\(id)?") { (data) in
             completion(NetworkParser.parsePersonDetail(data))
        }
    }
    
    func getPersonCredits(id: Int, completion: @escaping ([CreditMovie]) -> ()) {
        makeAPICall(path: "person/\(id)/movie_credits?") { (data) in
             completion(NetworkParser.parsePersonCredits(data))
        }
    }
    
    func getSearchResult(text: String, page: Int, completion: @escaping ([Any]) -> ()) {
        makeAPICall(path: "search/multi?query=\(text)&page=\(page)&") { (data) in
            print(page)
            completion(NetworkParser.parseSearchResult(data))
        }
    }
    
    private func makeAPICall(path: String, completion: @escaping (Dictionary<String, AnyObject>)-> ()) {
        let url = URL(string: String(format: NetworkManager.baseURL, path))
        
        Alamofire.request((url)!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) -> Void in
                    switch response.result {
                    case .success:
                        if let data = response.result.value as? Dictionary<String, AnyObject> {
                            completion(data)
                        }
  
                        break
                    case .failure:
                        print("Error")
                    }

                }

    }

    
}
