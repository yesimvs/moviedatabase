//
//  NetworkDataSource.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

protocol NetworkDataSource {
    
    func getMostPopularMovies(page: Int, completion: @escaping ([Movie])->())
    func getMovieDetail(id: Int, completion: @escaping (Detail)->())
    func getMovieCast(id: Int, completion: @escaping ([CastMember])->())
    func getMovieVideos(id: Int, completion: @escaping ([Video])->())
    func getPersonDetail(id: Int, completion: @escaping (Person?)->())
    func getPersonCredits(id: Int, completion: @escaping ([CreditMovie])->())
    func getSearchResult(text: String, page: Int, completion: @escaping ([Any])->())
}
