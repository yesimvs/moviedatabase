//
//  MovieDetailViewController.swift
//  ProjectMovie
//
//  Created by Yesim on 1/15/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import UIKit
import YouTubePlayer
import SVProgressHUD

// MARK: Viewcontroller life-cycle

class MovieDetailViewController: UIViewController {
    
    @IBOutlet weak var videoCollectionView: UICollectionView!
    @IBOutlet weak var castCollectionView: UICollectionView!
    @IBOutlet weak var movieSummaryLabel: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieRatingLabel: UILabel!
    @IBOutlet weak var movieNameLabel: UILabel!
    var videoPlayer = YouTubePlayerView()
    
    public var movieId : Int?
    var viewModel: MovieDetailViewModel = MovieDetailViewModel(dataSource: NetworkManager())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.castCollectionView.delegate = self
        self.castCollectionView.dataSource = self
        self.videoCollectionView.delegate = self
        self.videoCollectionView.dataSource = self
        self.castCollectionView.register(UINib(nibName: "CastCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CastCell")
        self.videoCollectionView.register(UINib(nibName: "TrailerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TrailerCell")
        self.addHomeButton()
        viewModel.delegate = self
        videoPlayer.frame = self.view.frame
        checkNeedReload()
        viewModel.getMovieDetails(id: self.movieId!)
    }
    
    func checkNeedReload() {
        viewModel.collectionNeedReload = { [weak self] in
            DispatchQueue.main.async {
                
                self?.castCollectionView.reloadData()
                self?.videoCollectionView.reloadData()
                self?.movieNameLabel.text = self!.viewModel.movieDetail?.title
                self?.movieSummaryLabel.text = self!.viewModel.movieDetail?.summary
                self?.movieRatingLabel.text = String(format:"%.1f", self?.viewModel.movieDetail?.rating ?? 0.00)
                self?.movieNameLabel.text = self!.viewModel.movieDetail?.title
                self?.movieImageView.image = UIImage(url: URL(string: self?.viewModel.movieDetail?.coverPhoto ?? ""))
            }
        }
    }


}

// MARK: Collectionview delegates

extension MovieDetailViewController : UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.isEqual(castCollectionView)){
          return self.viewModel.movieDetail?.cast?.count ?? 0
        }
        else {
            return self.viewModel.movieDetail?.videos?.count ?? 0
    }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width/4, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView.isEqual(castCollectionView)){
            let cell: CastCollectionCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "CastCell", for: indexPath) as? CastCollectionCell
            let cast : CastMember = viewModel.movieDetail!.cast![indexPath.row]
            cell?.castName.text = cast.originalName
            cell?.castName.adjustsFontSizeToFitWidth = true
            cell?.castName.minimumScaleFactor = 0.3
            cell?.castImageView.downloadImageFrom(link: cast.picturePath!, contentMode: UIView.ContentMode.scaleAspectFit)
            return cell!
        }
        else {
            let cell: TrailerCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "TrailerCell", for: indexPath) as? TrailerCollectionViewCell
            let trailer : Video = viewModel.movieDetail!.videos![indexPath.row]
            cell?.trailerImage.downloadImageFrom(link: trailer.imagePath!, contentMode: UIView.ContentMode.scaleAspectFit)
            return cell!
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView.isEqual(castCollectionView)){
            let person : CastMember = viewModel.cast[indexPath.row]
            let rootController = PersonDetailViewController()
            rootController.personId = person.id
            navigationController!.navigationBar.isTranslucent = false
            navigationController!.pushViewController(rootController, animated: true)
        }
        else{
            let trailer : Video = viewModel.movieDetail!.videos![indexPath.row]
            self.view.addSubview(videoPlayer)
            videoPlayer.loadVideoID(trailer.key!)
        }
    }

}

// MARK: Navigation bar customization

extension UIViewController {

    func addHomeButton() {
        let btnRightMenu: UIButton = UIButton()
        let image = UIImage(named: "HomeIcon");
        btnRightMenu.setImage(image, for: .normal)
        btnRightMenu.setTitle("Home", for: .normal);
        btnRightMenu.addTarget(self, action: #selector (homeButtonClick(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnRightMenu)
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationItem.rightBarButtonItem?.title = "Home"
    }

    @objc func homeButtonClick(sender : UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: Activity delegates

extension MovieDetailViewController: ViewModelDelegate {
    func willLoadData() {
        SVProgressHUD.show()
    }
    
    func didLoadData() {
        SVProgressHUD.dismiss()
    }
    
    
}
