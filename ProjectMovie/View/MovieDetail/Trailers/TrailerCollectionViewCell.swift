//
//  TrailerCollectionViewCell.swift
//  ProjectMovie
//
//  Created by Yesim on 1/16/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import UIKit

class TrailerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var trailerImage: UIImageView!
    
}
