//
//  MovieDetailViewModel.swift
//  ProjectMovie
//
//  Created by Yesim on 1/15/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

class MovieDetailViewModel: ViewModelType{
    var delegate: ViewModelDelegate?

    var collectionNeedReload: (()->())?
    var castNeedReload: (()->())?
    var trailerNeedReload: (()->())?
    private var dataSource: NetworkDataSource
    var videos: [Video] = [Video]()
    var cast: [CastMember] = [CastMember]()
 
    var movieDetail: Detail?{
        didSet {
//            delegate?.didLoadData()
//            self.collectionNeedReload?()
        }
    }

    init(dataSource: NetworkDataSource) {
          self.dataSource = dataSource
      }
    
    func getMovieDetails(id: Int) {
        delegate?.willLoadData()
        let group = DispatchGroup()

        group.enter()
        dataSource.getMovieDetail(id: id, completion: { [weak self] (detail) in
            group.leave()
            self?.movieDetail = detail
        })

        group.enter()
        dataSource.getMovieCast(id: id, completion: { [weak self] (cast) in
            self?.cast = cast
            group.leave()
        })
        
        group.enter()
          dataSource.getMovieVideos(id: id, completion: { [weak self] (videos) in
            self?.videos = videos
            group.leave()
          })

        group.notify(queue: .main) {
            self.movieDetail?.cast = self.cast
            self.movieDetail?.videos = self.videos
            self.delegate?.didLoadData()
            self.collectionNeedReload?()
        
        }
        
    }

}
