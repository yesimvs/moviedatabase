//
//  CastCollectionCell.swift
//  ProjectMovie
//
//  Created by Yesim on 1/15/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import UIKit

class CastCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var castName: UILabel!
    @IBOutlet weak var castImageView: UIImageView!
}
