//
//  PersonDetailViewController.swift
//  ProjectMovie
//
//  Created by Yesim on 1/16/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import UIKit
import SVProgressHUD

// MARK: Viewcontroller delegates

class PersonDetailViewController: UIViewController {

    @IBOutlet weak var personOverview: UILabel!
    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var creditCollectionView: UICollectionView!
    var viewModel: PersonDetailViewModel = PersonDetailViewModel(dataSource: NetworkManager())
    public var personId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        creditCollectionView.delegate = self
        creditCollectionView.dataSource = self
        creditCollectionView.register(UINib(nibName: "CreditCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CreditCell")
        navigationController?.navigationBar.topItem?.title = "Person Detail"
        viewModel.delegate = self
        viewModel.getPersonDetails(id: personId!)
        addHomeButton()
        checkNeedReload()

    }
    
    func checkNeedReload() {
        viewModel.dataNeedReload = { [weak self] in
            DispatchQueue.main.async {
               self?.creditCollectionView.reloadData()
               self?.personName.text = self!.viewModel.personDetail?.name
               self?.personOverview.text = self!.viewModel.personDetail?.biography
               self?.personImageView.image = UIImage(url: URL(string: self?.viewModel.personDetail?.picturePath ?? ""))
            }
        }
    }

}

// MARK: Collectionview delegates

extension PersonDetailViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.personDetail?.credits?.count ?? 0

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width/4, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CreditCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "CreditCell", for: indexPath) as? CreditCollectionViewCell
        let movie : CreditMovie = viewModel.personDetail!.credits![indexPath.row]
        cell?.castName.adjustsFontSizeToFitWidth = true
        cell?.castName.minimumScaleFactor = 0.2
        cell?.movieName.adjustsFontSizeToFitWidth = true
        cell?.movieName.minimumScaleFactor = 0.2
        cell?.castName.text = movie.castName
        cell?.movieName.text = movie.title
        cell?.movieImageView.downloadImageFrom(link: movie.imagePath!, contentMode: UIView.ContentMode.scaleAspectFit)
        return cell!

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie : CreditMovie = viewModel.personDetail!.credits![indexPath.row]
        let rootController = MovieDetailViewController()
        rootController.movieId = movie.id
        navigationController!.navigationBar.isTranslucent = false
        navigationController!.pushViewController(rootController, animated: true)
    }
 
}

// MARK: Activity delegates

extension PersonDetailViewController : ViewModelDelegate {
    func willLoadData() {
        SVProgressHUD.show()
    }
    
    func didLoadData() {
        SVProgressHUD.dismiss()
    }
    
    
}

