//
//  CreditCollectionViewCell.swift
//  ProjectMovie
//
//  Created by Yesim on 1/16/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import UIKit

class CreditCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var castName: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieName: UILabel!
}
