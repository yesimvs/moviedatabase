//
//  PersonDetailViewModel.swift
//  ProjectMovie
//
//  Created by Yesim on 1/16/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

class PersonDetailViewModel: ViewModelType{
    var delegate: ViewModelDelegate?
    var credits: [CreditMovie] = [CreditMovie]()
    var dataNeedReload: (()->())?
    private var dataSource: NetworkDataSource
 
    var personDetail: Person?{
        didSet {
//            delegate?.didLoadData()
//            self.aboutNeedReload?()
        }
    }

    init(dataSource: NetworkDataSource) {
          self.dataSource = dataSource
      }
    
    func getPersonDetails(id: Int) {
        delegate?.willLoadData()
        let group = DispatchGroup()

        group.enter()
        dataSource.getPersonDetail(id: id, completion: { [weak self] (detail) in
            group.leave()
            self?.personDetail = detail
        })

        group.enter()
        dataSource.getPersonCredits(id: id, completion: { [weak self] (credits) in
            self?.credits = credits
            group.leave()
        })


        group.notify(queue: .main) {
            self.personDetail?.credits = self.credits
            self.delegate?.didLoadData()
            self.dataNeedReload?()
    
        }
        
    }

}
