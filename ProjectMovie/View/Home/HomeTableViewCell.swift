//
//  HomeTableViewCell.swift
//  ProjectMovie
//
//  Created by Yesim on 1/15/20.
//  Copyright © 2020 Yesim. All rights reserved.
//

import UIKit

// MARK: Custom cell for home tableview

class HomeTableViewCell: UITableViewCell {
    
    enum type {
        case movie
        case person
    }

    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    var mediaType : HomeTableViewCell.type = .movie
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
