//
//  HomeViewController.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import UIKit
import SVProgressHUD

// MARK: extensions for image loading and lazy loading

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}

extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
                if error != nil {
                    self.image = UIImage(imageLiteralResourceName: "GuestIcon")
                }
            }
        }).resume()
    }
}

// MARK: View controller life-cycle

class HomeViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var intPageNum = 1
    var viewModel: HomeViewModel = HomeViewModel(dataSource: NetworkManager())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.topItem?.title = "Home"
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        viewModel.delegate = self
        tableView.rowHeight = 160.0
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        
        checkNeedReload()
        viewModel.initProgress()

    }
    
    func checkNeedReload() {

        viewModel.tableNeedReload = { [weak self] in
            DispatchQueue.main.async {
                self!.tableView.reloadData()
                
                if(self?.viewModel.errorString.isEmpty==false){
                    let alert = UIAlertController(title: "Alert", message: self!.viewModel.errorString, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                          switch action.style{
                          case .default:
                            self?.viewModel.errorString = ""
                                print("default")
                          @unknown default: break
                            
                        }}))
                    self!.present(alert, animated: true, completion: nil)
                }
            }
        }

    }

}

// MARK: Tableview delegates

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(viewModel.isOnSearchView){
            if(section==0){
                return viewModel.searchMovies.count
            }
            else {
                return viewModel.searchPeople.count
            }
        }
        else {
           return viewModel.movies.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(viewModel.isOnSearchView){
            if viewModel.searchPeople.count > 0 {
               return 2
            }
            else {
               return 1
            }
        }
        else {
           return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      if(viewModel.isOnSearchView) {
        return viewModel.sectionString[section]
      }
        return ""
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:HomeTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as? HomeTableViewCell
        if cell == nil {
            cell = HomeTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "HomeCell")
        }
        if(viewModel.isOnSearchView){
            switch (indexPath.section) {

            case 0:
            let movie : Movie = viewModel.searchMovies[indexPath.row]
            cell?.movieTitle.text = movie.title
            cell?.moviePoster.downloadImageFrom(link: movie.imagePath!, contentMode: UIView.ContentMode.scaleAspectFit)
            case 1:
            let person : CastMember = viewModel.searchPeople[indexPath.row]
            cell?.movieTitle.text = person.originalName
            cell?.moviePoster.downloadImageFrom(link: person.picturePath!, contentMode: UIView.ContentMode.scaleAspectFit)
            default:
             cell?.movieTitle.text = ""
            cell?.moviePoster.downloadImageFrom(link: "", contentMode: UIView.ContentMode.scaleAspectFit)

            }

        }
        else {
            let movie : Movie = viewModel.movies[indexPath.row]
            cell?.movieTitle.text = movie.title
            cell?.moviePoster.downloadImageFrom(link: movie.imagePath!, contentMode: UIView.ContentMode.scaleAspectFit)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(viewModel.isOnSearchView){
            if(indexPath.section==0){
                let movie : Movie = viewModel.searchMovies[indexPath.row]
                let rootController = MovieDetailViewController()
                rootController.movieId = movie.id
                navigationController!.pushViewController(rootController, animated: true)
            }
            else {
                let person : CastMember = viewModel.searchPeople[indexPath.row]
                let rootController = PersonDetailViewController()
                rootController.personId = person.id
                navigationController!.pushViewController(rootController, animated: true)
            }
        }
        else {
            let movie : Movie = viewModel.movies[indexPath.row]
            let rootController = MovieDetailViewController()
            rootController.movieId = movie.id
            navigationController!.pushViewController(rootController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            intPageNum = intPageNum + 1
            self.viewModel.tableViewNeedFetchMore(_pageNum: intPageNum)
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
        }
    }
    
 
}

// MARK: Search bar delegates

extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.getSearchResult(_searchText: searchBar.text ?? "", pageNum: intPageNum)
        intPageNum = 1
        self.searchBar.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        viewModel.searchIsStartedAgain(_isStart: true)
        searchBar.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       
        searchBar.text = ""
        viewModel.searchIsStartedAgain(_isStart: true)
        self.searchBar.endEditing(true)
    }
}

// MARK: Activity delegates

extension HomeViewController: ViewModelDelegate {
    func willLoadData() {
        SVProgressHUD.show()
    }
    func didLoadData() {
        SVProgressHUD.dismiss()
    }
}
