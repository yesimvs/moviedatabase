//
//  HomeViewModel.swift
//  ProjectMovie
//
//  Created by Yesim on 14.01.2020.
//  Copyright © 2020 Yesim. All rights reserved.
//

import Foundation

extension String {
   public var fixWhiteSpaces: String {
      let modified = self.replacingOccurrences(of: " ", with: "%20")
      return modified
   }
}

class HomeViewModel: ViewModelType{
    
    var sections: [(name: String, rows: [ResultItemModel])] = []
    var delegate: ViewModelDelegate?
    var isOnSearchView : Bool = false
    var tableNeedReload: (()->())?
    var data = [[String: Any]]()
    var lastSearchKey : String = String()
    private var dataSource: NetworkDataSource
    var searchMovies = [Movie]()
    var searchPeople = [CastMember]()
    let sectionString : [String] = ["Movies","People"];

    var movies: [Movie] = [Movie]() {
        didSet {
            delegate?.didLoadData()
            self.tableNeedReload?()
        }
    }
    var searchResults: [Any] = [Any]() {
        didSet {
            self.isOnSearchView = true
            self.tableNeedReload?()
        }
    }
    
    var errorString: String = String() {
        didSet {
            self.isOnSearchView = true
            self.searchIsDismissed(_isDismiss: true)
        }
    }
    
    func initProgress (){
        getMostPopularMovies(page: 1)
    }
    
    init(dataSource: NetworkDataSource) {
        self.dataSource = dataSource
    }
    
    var numberOfMovies: Int {
        return movies.count
    }
    
    func searchIsStartedAgain(_isStart: Bool) {
    if(_isStart == true){
        self.isOnSearchView = false
        self.tableNeedReload?()
        }
    }
    
    func getMostPopularMovies (page: Int){
        delegate?.willLoadData()
        dataSource.getMostPopularMovies(page: page,completion: gotMovies(_:))
    }
    
    func searchIsDismissed (_isDismiss: Bool){
        if _isDismiss == true {
            self.isOnSearchView = false
            self.tableNeedReload?()
        }
    }
    
    func getSearchResult (_searchText: String, pageNum: Int){
        if lastSearchKey != _searchText {
            lastSearchKey = _searchText
            self.searchMovies.removeAll()
            self.searchPeople.removeAll()
            self.searchResults.removeAll()
        }
        dataSource.getSearchResult(text: _searchText.fixWhiteSpaces, page: pageNum ,completion: gotResults(_:))
    }
    
    func tableViewNeedFetchMore (_pageNum: Int){
        if isOnSearchView {
            getSearchResult(_searchText: lastSearchKey, pageNum: _pageNum)
        }
        else {
            getMostPopularMovies(page: _pageNum)
        }
    }
    
    private func gotMovies(_ movies: [Movie]){
        if(self.movies.count != 0){
            for newMovie in movies {
                self.movies.append(newMovie)
            }
        }
        else {
           self.movies = movies
        }
     }
    
    private func gotResults(_ results: [Any]){
        
        if(results.count==0){
            self.errorString = "No result"
          }
        else {
            if(self.searchResults.count != 0){

                for movie in results.compactMap({ $0 as? Movie }) {
                    searchMovies.append(movie)
                }
                for cast in results.compactMap({ $0 as? CastMember }) {
                    searchPeople.append(cast)
                }
                self.searchResults = results
            }
            else {
               searchMovies = results.compactMap { $0 as? Movie }
               searchPeople = results.compactMap { $0 as? CastMember }
               self.searchResults = results
            }

        }

    }


}
